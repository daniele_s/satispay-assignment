import React from "react";
import { IPokemon } from "../models/pokemon";
import { Table, Button } from "antd";
import { ColumnProps } from "antd/lib/table";
import PokemonType from "./PokemonType";
import style from "../style/results-table-component.module.scss";
import { PokemonType as PType } from "../models/types";

interface IResultsTableProps {
  results: IPokemon[];
  fetchMore: () => void;
  hasNextPage: boolean;
  loading?: boolean;
}

interface ITableReloaderProps {
  fetchMore: () => void;
}

const tableColumns: ColumnProps<IPokemon>[] = [
  {
    title: "Num",
    dataIndex: "id",
    width: 100
  },
  {
    title: "",
    key: "image",
    width: 100,
    render: (text, record) => (
      <img
        src={`https://veekun.com/dex/media/pokemon/main-sprites/heartgold-soulsilver/${parseInt(
          record.id
        )}.png`}
        className={style.image}
      />
    )
  },
  {
    title: "Name",
    dataIndex: "name"
  },
  {
    title: "Type",
    key: "type",
    render: (text, record) => (
      <>
        {record.types.map((type) => (
          <PokemonType key={type} type={type.toLowerCase() as PType} />
        ))}
      </>
    )
  },
  {
    title: "Classification",
    dataIndex: "classification",
    responsive: ["md"]
  } as any
];

function TableReloader({ fetchMore }: ITableReloaderProps) {
  return (
    <div className={style.tableReloaderWrapper}>
      <Button onClick={fetchMore}>Fetch more results</Button>
    </div>
  );
}

export default function ResultsTable({
  results,
  fetchMore,
  hasNextPage,
  loading = false
}: IResultsTableProps) {
  return (
    <div className={style.tableWrapper}>
      <Table
        dataSource={results}
        rowKey="id"
        loading={loading}
        columns={tableColumns}
        pagination={false}
        className={style.table}
      />
      {hasNextPage && <TableReloader fetchMore={fetchMore} />}
    </div>
  );
}
