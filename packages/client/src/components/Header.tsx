import React from "react";
import { Typography, Layout } from "antd";

import style from "../style/header-component.module.scss";
import { primaryColor } from "../style/colors";

const { Title } = Typography;

export default function Header() {
  return (
    <Layout.Header className={style.header}>
      <img src="/assets/logo.png" alt="Pokéball" className={style.logo} />
      <Title style={{ color: primaryColor, marginBottom: 0 }} level={2}>
        Pokedex
      </Title>
    </Layout.Header>
  );
}
