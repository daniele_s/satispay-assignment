import React, { useState } from "react";
import { Layout, Input, Button, Divider } from "antd";
import { PokemonType as PType } from "../models/types";
import { pokemonTypes } from "../data/types";
import classnames from "classnames";
import { RollbackOutlined, SearchOutlined } from "@ant-design/icons";

import style from "../style/search-bar-component.module.scss";
import { Typography } from "antd";
import PokemonType, { PokemonTypeSize } from "./PokemonType";

interface ISearchBarProps {
  onSearch: (query: string, type?: PType | null) => void;
  onReset: () => void;
}

interface ITypeSelectorProp {
  onChange: (type: PType | null) => void;
  type: PType | null;
}

function TypeSelector({ onChange, type: selectedType }: ITypeSelectorProp) {
  const onSelectType = (type: PType) => () => {
    if (type === selectedType) {
      onChange(null);
    } else {
      onChange(type);
    }
  };

  return (
    <div className={style.typesWrapper}>
      {pokemonTypes.map((type) => (
        <PokemonType
          type={type}
          key={type}
          size={PokemonTypeSize.SMALL}
          onClick={onSelectType(type)}
          className={classnames(style.type, {
            [style.typeNotSelected]: selectedType && selectedType !== type
          })}
        />
      ))}
    </div>
  );
}

export default function SearchBar({ onSearch, onReset }: ISearchBarProps) {
  const [query, setQuery] = useState<string>("");
  const [type, setType] = useState<PType | null>(null);

  const onSubmit = () => {
    onSearch(query, type);
  };

  return (
    <div className={style.searchWrapper}>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          onSubmit();
        }}
      >
        <div className={style.titleWrapper}>
          <Typography.Title level={4}>Search</Typography.Title>
          <Button
            onClick={() => {
              setType(null);
              setQuery("");
              onReset();
            }}
            icon={(<RollbackOutlined />) as any}
            shape="round"
          >
            Reset
          </Button>
        </div>
        <section className={style.section}>
          <h5 className={style.title}>Name</h5>
          <Input
            placeholder="Pikachu..."
            value={query}
            onChange={(e) => setQuery(e.target.value)}
            addonAfter={<SearchOutlined />}
          />
        </section>
        <section className={style.section}>
          <h5 className={style.title}>Type</h5>
          <TypeSelector onChange={(type) => setType(type)} type={type} />
        </section>
        <div className={style.buttonWrapper}>
          <Button type={"primary"} htmlType="submit">
            Search
          </Button>
        </div>
      </form>
    </div>
  );
}
