import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import { apolloClient } from "../modules/apollo";
import { Layout } from "antd";
import Header from "./Header";
import SearchPage from "./SearchPage";

export default function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <Layout>
        <Header />
        <SearchPage />
      </Layout>
    </ApolloProvider>
  );
}
