import React from "react";
import style from "../style/pokemon-type-component.module.scss";
import { PokemonType } from "../models/types";
import tinycolor from "tinycolor2";
import classnames from "classnames";

export enum PokemonTypeSize {
  SMALL,
  LARGE
}

interface IPokemonType {
  type: PokemonType;
  size?: PokemonTypeSize;
  onClick?: () => void;
  className?: string;
}

const typeColorMap: Map<PokemonType, string> = new Map<PokemonType, string>([
  ["bug", "#a8b820"],
  ["dark", "#705848"],
  ["dragon", "#7038f8"],
  ["electric", "#f8d030"],
  ["fairy", "#dea5de"],
  ["fighting", "#c03028"],
  ["fire", "#f08030"],
  ["flying", "#a890f0"],
  ["ghost", "#705898"],
  ["grass", "#78c850"],
  ["ground", "#e0c068"],
  ["normal", "#a8a878"],
  ["ice", "#98d8d8"],
  ["poison", "#a040a0"],
  ["psychic", "#f85888"],
  ["rock", "#b8a038"],
  ["steel", "#b8b8d0"],
  ["water", "#6890f0"]
]);

export default function PokemonType({
  type,
  size = PokemonTypeSize.LARGE,
  className = "",
  onClick = () => {}
}: IPokemonType) {
  const typeColor = typeColorMap.get(type);
  return (
    <div
      className={classnames(style.typeWrapper, className, {
        [style.small]: size === PokemonTypeSize.SMALL
      })}
      style={{
        border: `1px solid ${typeColor}`,
        backgroundColor: tinycolor(typeColor).lighten(20).toHex8String()
      }}
      onClick={onClick}
    >
      {type.toUpperCase()}
    </div>
  );
}
