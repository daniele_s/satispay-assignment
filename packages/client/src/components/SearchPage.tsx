import React from "react";
import { Layout } from "antd";
import ResultsTable from "./ResultsTable";
import SearchBar from "./SearchBar";

import style from "../style/search-page-component.module.scss";
import { usePokemonApi } from "../hooks/use-pokemon-api";
import { PokemonType } from "../models/types";

export default function SearchPage() {
  const { error, data, search, loading, ...rest } = usePokemonApi();

  const scrollTop = () => {
    window.scrollTo(0, 0);
  };

  return (
    <Layout.Content>
      <div className={style.pageWrapper}>
        <div className={style.searchBar}>
          <SearchBar
            onSearch={(query: string, type?: PokemonType | null) => {
              scrollTop();
              search(query, type);
            }}
            onReset={() => {
              scrollTop();
              search("");
            }}
          />
        </div>
        <div className={style.contentWrapper}>
          {data?.length || loading ? (
            <ResultsTable results={data} loading={loading} {...rest} />
          ) : (
            <div className={style.noResultsWrapper}>
              <img src="/assets/sad_cubone.png" />
              <span>No results found</span>
            </div>
          )}
        </div>
      </div>
    </Layout.Content>
  );
}
