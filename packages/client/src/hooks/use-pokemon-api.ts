import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { useState, useEffect } from "react";
import { PokemonType } from "../models/types";

const GET_POKEMON = gql`
  query($q: String, $type: String, $after: ID) {
    pokemons(q: $q, type: $type, after: $after) {
      edges {
        cursor
        node {
          id
          name
          types
          classification
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

export function usePokemonApi() {
  const { data, fetchMore, refetch, ...rest } = useQuery(GET_POKEMON);

  return {
    ...rest,
    data: data?.pokemons.edges.map((edge: any) => edge.node) ?? [],
    hasNextPage: data?.pokemons.pageInfo.hasNextPage ?? false,
    search: (query: string, type?: PokemonType | null) =>
      refetch({ q: query, type: type === null ? undefined : type }),
    fetchMore: () =>
      fetchMore({
        variables: {
          after: data.pokemons.pageInfo.endCursor
        },
        updateQuery(previousResult, { fetchMoreResult }) {
          const newEdges = fetchMoreResult.pokemons.edges;
          const pageInfo = fetchMoreResult.pokemons.pageInfo;

          return newEdges.length
            ? {
                pokemons: {
                  __typename: previousResult.pokemons.__typename,
                  edges: [...previousResult.pokemons.edges, ...newEdges],
                  pageInfo
                }
              }
            : previousResult;
        }
      })
  };
}
