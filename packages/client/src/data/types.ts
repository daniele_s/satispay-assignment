import { PokemonType } from "../models/types";

export const pokemonTypes: PokemonType[] = [
  "steel",
  "fairy",
  "dark",
  "dragon",
  "ghost",
  "rock",
  "bug",
  "psychic",
  "flying",
  "ground",
  "poison",
  "fighting",
  "ice",
  "grass",
  "electric",
  "water",
  "fire",
  "normal"
];
