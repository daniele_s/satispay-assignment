const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

function getStyleLoaders(isProd, modules, isSass = true) {
  return [
    {
      loader: "style-loader"
    },
    {
      loader: "css-loader",
      options: {
        modules,
        sourceMap: !isProd
      }
    },
    {
      loader: "postcss-loader",
      options: {
        ident: "postcss",
        plugins: () => [require("autoprefixer")]
      }
    },
    isSass
      ? {
          loader: "sass-loader",
          options: { implementation: require("sass") }
        }
      : {
          loader: "less-loader",
          options: {
            lessOptions: {
              modifyVars: { "@primary-color": "rgb(222, 6, 47)" },
              javascriptEnabled: true
            }
          }
        }
  ].filter(Boolean);
}

module.exports = (env, { mode }) => {
  const isProd = mode === "production";
  return {
    mode,
    entry: path.resolve(__dirname, "src/index.tsx"),
    output: {
      filename: "static/js/bundle.js",
      chunkFilename: "static/js/[name].chunk.js",
      path: path.resolve(__dirname, "dist")
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    devtool: !isProd ? "source-map" : "none",
    module: {
      rules: [
        {
          test: /\.m?(j|t)sx?$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: "ts-loader"
          }
        },
        {
          test: /\.s?css$/,
          exclude: /\.module\.scss/,
          use: getStyleLoaders(isProd, false)
        },
        {
          test: /\.module\.s?css/,
          use: getStyleLoaders(isProd, true)
        },
        {
          test: /\.less$/,
          use: getStyleLoaders(isProd, false, false)
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: true,
        template: path.resolve(__dirname, "public/index.html"),
        collapseWhitespace: true
      })
    ],
    optimization: {
      minimize: isProd,
      minimizer: [new TerserPlugin()],
      splitChunks: {
        chunks: "all",
        name: true
      },
      runtimeChunk: true
    },
    devServer: {
      contentBase: path.join(__dirname, "public"),
      compress: true,
      port: 8082,
      watchContentBase: true
    }
  };
};
